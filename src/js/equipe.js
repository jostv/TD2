class Equipe {

    constructor(nom) {
        this.nom = nom
        this.classement = 1
        this.nbMatchGagnes = 0
        this.nbMatchNuls = 0
        this.nbMatchPerdus = 0
        this.nbButsMarques = 0
        this.nbButsEncaisses = 0
        this.nbAlea = Math.random()
    }

    /**
     * Compare deux équipes passées en argument selon les critères du classement.
     * Les équipes sont comparées selon leur nombre de points, puis en cas d'égalité selon leur différence de buts, puis en cas
     * d'égalité selon leur nombre de buts marqués. En cas d'égalité complète, elles sont départagées en fonction
     * de la valeur aléatoire générée à la création de chaque équipe.
     *
     * @param {Equipe} eq1 une équipe du championnat
     * @param {Equipe} eq2 une équipe du championnat
     * @return {Number} une valeur strictement négative si eq2 est devant eq1, une valeur strictement positive si
     * eq1 est devant eq2, 0 si eq1 === eq2.
     */
    static compare(eq1, eq2) {
        let scoreEq1 = eq1.nbPoints() * 10000 + (eq1.nbButsMarques-eq1.nbButsEncaisses) * 100 + eq1.nbButsMarques+ eq1.nbAlea
        let scoreEq2 = eq2.nbPoints() * 10000 + (eq2.nbButsMarques-eq2.nbButsEncaisses) * 100 + eq2.nbButsMarques+ eq2.nbAlea
        return scoreEq1-scoreEq2;
    }

    nbPoints() {
        return 3*this.nbMatchGagnes + this.nbMatchNuls
    }

    toString() {
        return `${this.classement}   ${this.nom}  ${this.nbPoints()}  ${this.nbMatchGagnes}  ${this.nbMatchNuls}  ${this.nbMatchPerdus}  ${this.nbButsMarques}  ${this.nbButsEncaisses}  ${this.nbButsMarques-this.nbButsEncaisses}`
    }

    miseAJour(nbButsMarques, nbButsEncaisses) {
        this.nbButsMarques += nbButsMarques
        this.nbButsEncaisses += nbButsEncaisses
        // Victoire
        if (nbButsMarques > nbButsEncaisses)
            this.nbMatchGagnes += 1
        // Match nul
        else if (nbButsMarques === nbButsEncaisses)
            this.nbMatchNuls += 1
        // Défaite
        else this.nbMatchPerdus += 1
    }
}
